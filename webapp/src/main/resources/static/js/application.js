const data = {
    selfId: selfId,
    filter: {
        tag: ""
    },
    orderType: "2",
    order: {
        name: "",
        type: ""
    },
    pageNumber: 0,
    limit: 10,
    page: {},
    modalActive: {
        link: "",
        tags: []
    },
    modalPage: {},
    modalContent: {},
    newTag: ""
};

Vue.component('card', {
    props: {
        data: {},
        index: 0,
        page: 0
    },
    template:
        '<div class="card">' +
        '   <img class="card-img-top" v-bind:src="data.link" alt="Image" @click="openModal">' +
        '   <div class="card-body">' +
        '       <button type="button" class="btn btn-secondary bmd-btn-icon" v-if="renderTag" @click="openTagModal" data-toggle="modal" data-target="#tagEditModel">' +
        '           <i class="fas fa-hashtag"></i>' +
        '       </button>' +
        '       <button type="button" class="btn btn-danger bmd-btn-icon" @click="like">' +
        '           <i class="fa-heart" v-bind:class="liked" style="color: #ff6b6b;"></i>' +
        '       </button>' +
        '       {{ data.likeCount }} отметок "Нравится"' +
        '   </div>' +
        '</div>',
    methods: {
        like: function () {
            if (!logged) {
                return;
            }

            this.data.liked = !this.data.liked;
            axios.post("/api/images/" + this.data.id + "/like")
                .then(response => this.data.likeCount = response.data.likeCount);
        },
        openModal: function () {
            openPopup(this.index, this.page);
        },
        openTagModal: function () {
            data.modalActive = this.data;
            $('#tagEditModal').modal('show');
        }
    },
    computed: {
        liked: function () {
            return {far: !this.data.liked, fas: this.data.liked || !logged};
        },
        renderTag: function () {
            return logged && data.selfId === parseInt(this.data.author, 10);
        },
    }
});


Vue.component('cardlist', {
    props: {
        cards: {},
        pagenumber: 0
    },
    template:
        '<div class="card-columns">' +
        '   <div v-for="(item, index) in this.cards">' +
        '       <card v-bind:data="item" v-bind:index="index" v-bind:page="pagenumber" :key="item.id"></card>' +
        '   </div>' +
        '</div>'
});

Vue.component('pagination', {
    props: {
        page: {}
    },
    template:
        '<nav v-if="page.totalPages > 1">' +
        '   <ul class="pagination justify-content-center">' +
        '       <li class="page-item" v-bind:class="{ disabled: page.first }" @click="changePage(0)"><a class="page-link" href="#">Начало</a></li>' +
        '       <li class="page-item" v-bind:class="{ active: (page.number + 1 === index) }" v-for="index in page.totalPages" :key="index" v-if="page.number - 2 < index && page.number + 4 > index" @click="changePage(index - 1)"><a class="page-link" href="#">{{ index }}</a></li>'
        +
        '       <li class="page-item" v-bind:class="{ disabled: page.last }" @click="changePage(page.totalPages - 1)"><a class="page-link" href="#">Конец</a></li>'
        +
        '   </ul>' +
        '</nav>',
    methods: {
        changePage: function (index) {
            data.pageNumber = index;
            app.search();
        }
    }
});

function creatQuery(filter, order, pageNumber, limit) {
    const params = [];
    if (filter.tag) {
        params.push("tag=" + filter.tag);
    }
    if (pageNumber) {
        params.push("offset=" + pageNumber);
    }
    if (limit) {
        params.push("limit=" + limit);
    }
    if (order.name) {
        params.push("order=" + order.name);
    }
    if (order.type) {
        params.push("orderType=" + order.type);
    }
    return params.join("&");
}

const app = new Vue({
    el: '#app',
    data: data,
    methods: {
        updateOrder: function () {
            if (this.orderType === "1") {
                this.order.name = "likeCount";
                this.order.type = "asc";
            }
            if (this.orderType === "2") {
                this.order.name = "likeCount";
                this.order.type = "desc";
            }
            if (this.orderType === "3") {
                this.order.name = "name";
                this.order.type = "asc";
            }
            if (this.orderType === "4") {
                this.order.name = "name";
                this.order.type = "desc";
            }
        },
        search: function () {
            this.updateOrder();
            axios.get("/api/images?" + creatQuery(this.filter, this.order, this.pageNumber, this.limit))
                .then(function (pageResponse) {
                    const page = pageResponse.data;
                    const liked = page.content.map((c) => {
                        return c.id;
                    });
                    if (!logged) {
                        data.page = page;
                        data.modalPage = page;
                        return;
                    }
                    axios.get("/api/users/self/likes?ids=" + liked)
                        .then(function (response) {
                            page.content.forEach(function (item) {
                                item.liked = response.data.includes(item.id);
                            });
                            data.page = page;
                            data.modalPage = page;
                        });
                });
        },
        searchByTag: function (tag) {
            this.filter.tag = tag;
            $('#tagFilterGroup').addClass('is-filled');
            $('#tagEditModal').modal('hide');
            this.search();
        },
        addNewTag: function () {
            if (this.newTag !== "") {
                axios.post("/api/images/" + this.modalActive.id + "/tags", {text: this.newTag})
                    .then(function (response) {
                        data.newTag = "";
                        $('#newTagInputGroup').removeClass('is-filled');
                        data.modalActive.tags = response.data.tags;
                    });
            }

        }
    },
    updated: function () {
        $('.form-inline').bootstrapMaterialDesign();
    }
});

app.search();
subscribeToEvents();

let fileData = new FormData();
$('#uploadButton').click(function () {
    axios.post('/upload', fileData, {
        headers: {
            'Content-Type': 'multipart/form-data',
        }
    }).then(function () {
        $('#uploadModal').modal('hide');
        const options = {
            content: "Загрузка изображения началась",
            style: "toast",
            timeout: 3000
        };
        $.snackbar(options);
    });
});

$('#fileUpload').change(function (e) {
    let files = e.target.files || e.dataTransfer.files;
    if (!files.length) {
        return
    }
    fileData = new FormData();
    fileData.append('file', files[0]);
});

function subscribeToEvents() {
    if (!logged) {
        return;
    }

    const socket = new SockJS('/ws');
    const stompClient = Stomp.over(socket);
    stompClient.debug = null;
    stompClient.connect({}, function (frame) {
        stompClient.subscribe('/user/topic/uploadEvent', function (data) {
            showToast(JSON.parse(data.body).success);
        });
    });
}

function showToast(success) {
    let message = "Изображение успешно загружено";
    if (!success) {
        message = "Произошла ошибка при загрузке изображения";
    }
    const options = {
        content: message,
        style: "toast",
        timeout: 3000
    };
    $.snackbar(options);
}

function openPopup(index, pageNumber) {
    data.pageNumber = pageNumber;
    axios.get("/api/images?" + creatQuery(data.filter, data.order, pageNumber, data.limit))
        .then(function (pageResponse) {
            const page = pageResponse.data;
            const liked = page.content.map((c) => {
                return c.id;
            });
            if (!logged) {
                data.modalPage = page;
                const items = data.modalPage.content.map(function (el) {
                    return {
                        src: el.link,
                        title: el.tags.join(', ')
                    }
                });
                $.magnificPopup.open(
                    {
                        items: items,
                        gallery: {
                            enabled: true
                        },
                        type: 'image'
                    }, index);
                return;
            }
            axios.get("/api/users/self/likes?ids=" + liked)
                .then(function (response) {
                    page.content.forEach(function (item) {
                        item.liked = response.data.includes(item.id);
                    });
                    data.modalPage = page;
                    const items = data.modalPage.content.map(function (el) {
                        return {
                            src: el.link,
                            title: el.tags.join(', ')
                        }
                    });
                    $.magnificPopup.open(
                        {
                            items: items,
                            gallery: {
                                enabled: true
                            },
                            type: 'image'
                        }, index);
                });
        });

}


$.magnificPopup.instance.next = function () {
    const index = $.magnificPopup.instance.index;
    if (index + 1 === data.modalPage.numberOfElements) {
        if (!data.modalPage.last) {
            openPopup(0, data.pageNumber + 1);
        }
    }
    $.magnificPopup.proto.next.call(this);
};
$.magnificPopup.instance.prev = function () {
    const index = $.magnificPopup.instance.index;

    if (index === 0) {
        if (!data.modalPage.first) {
            openPopup(data.modalPage.content.length - 1, data.pageNumber - 1);
        }
        return;
    }
    $.magnificPopup.proto.prev.call(this);
};
package com.asrocket.tulawebcup.webapp.controller

import com.asrocket.tulawebcup.webapp.ImageMessage
import org.apache.commons.codec.digest.Md5Crypt
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.support.MessageBuilder
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.multipart.MultipartFile
import java.util.concurrent.TimeUnit

@Controller
class HomeController(val redisTemplate: RedisTemplate<String, ByteArray>,
                     val output: MessageChannel) {

    @GetMapping("/")
    fun getIndexPage(principal: OAuth2Authentication?, model: Model): String {
        model.addAttribute("logged", principal != null)
        if (principal != null) {
            model.addAttribute("selfId", principal.name)
            val map = principal.userAuthentication.details as Map<String, String>
            model.addAttribute("avatarId", map["default_avatar_id"])
        } else {
            model.addAttribute("selfId", 0)
        }
        return "index"
    }

    @ResponseBody
    @PostMapping("/upload")
    fun uploadFile(@RequestParam("file") file: MultipartFile,
                   authentication: OAuth2Authentication) {
        val md5Crypt = Md5Crypt.md5Crypt((file.name + authentication.name).toByteArray())
        redisTemplate.expire("images", 10, TimeUnit.MINUTES)
        redisTemplate.opsForHash<String, ByteArray>().put("images", md5Crypt, file.bytes)
        val token = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        output.send(MessageBuilder.withPayload(ImageMessage(md5Crypt, authentication.name, token, file.originalFilename!!, file.contentType!!)).build())
    }
}
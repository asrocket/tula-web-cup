package com.asrocket.tulawebcup.webapp.client

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader

@FeignClient(name = "user-info-client", url = "https://login.yandex.ru/info")
interface YandexUserInfoClient {

    @GetMapping
    fun getUserInfo(@RequestHeader("Authorization") auth: String): Map<String, Any>
}
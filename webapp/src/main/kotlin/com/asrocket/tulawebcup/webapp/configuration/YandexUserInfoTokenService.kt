package com.asrocket.tulawebcup.webapp.configuration

import com.asrocket.tulawebcup.webapp.client.YandexUserInfoClient
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.OAuth2Request
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices
import java.util.*

class YandexUserInfoTokenService(
        private val clientId: String,
        private val yandexUserInfoClient: YandexUserInfoClient) : ResourceServerTokenServices {
    private val principalExtractor = PrincipalExtractor { it["id"] }

    override fun loadAuthentication(accessToken: String): OAuth2Authentication {
        val map = getMap(accessToken)
        if (map == null || map.containsKey("error")) {
            throw InvalidTokenException(accessToken)
        }
        return extractAuthentication(map)
    }

    private fun extractAuthentication(map: Map<String, *>): OAuth2Authentication {
        val principal = getPrincipal(map)
        val request = OAuth2Request(null, clientId, null, true, null, null, null, null, null)
        val token = UsernamePasswordAuthenticationToken(principal, "N/A", listOf())
        token.details = map
        return OAuth2Authentication(request, token)
    }

    private fun getPrincipal(map: Map<String, *>) = principalExtractor.extractPrincipal(map) ?: "unknown"


    private fun getMap(accessToken: String): Map<String, *>? = try {
        yandexUserInfoClient.getUserInfo("OAuth $accessToken")
    } catch (ex: Exception) {
        Collections.singletonMap<String, Any>("error", "Could not fetch user details")
    }

    override fun readAccessToken(accessToken: String?): OAuth2AccessToken {
        throw UnsupportedOperationException("Not supported: read access token")
    }


}
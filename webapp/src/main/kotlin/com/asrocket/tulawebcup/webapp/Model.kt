package com.asrocket.tulawebcup.webapp

data class ImageMessage(val id: String, val author: String, val token: String, val name: String, val mimeType: String)

data class EventMessage(val username: String, val success: Boolean = true)
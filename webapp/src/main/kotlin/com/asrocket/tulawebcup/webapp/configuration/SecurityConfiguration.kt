package com.asrocket.tulawebcup.webapp.configuration

import com.asrocket.tulawebcup.webapp.client.YandexUserInfoClient
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoRestTemplateFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@Configuration
@EnableOAuth2Sso
class SecurityConfiguration(val sso: ResourceServerProperties,
                            val yandexUserInfoClient: YandexUserInfoClient) : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity) {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "/api/**", "/js/**", "/css/**", "/fonts/**", "/favicon.ico", "/ws/**").permitAll()
                .anyRequest().authenticated()
        http.logout()
                .logoutSuccessUrl("/").permitAll()
    }

    @Bean
    @Primary
    fun yandexUserInfoTokenService(restTemplateFactory: UserInfoRestTemplateFactory) =
            YandexUserInfoTokenService(this.sso.clientId, yandexUserInfoClient)

}
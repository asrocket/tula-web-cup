package com.asrocket.tulawebcup.webapp

import com.netflix.zuul.ZuulFilter
import com.netflix.zuul.context.RequestContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails
import org.springframework.stereotype.Component

@Component
class AuthHeaderFilter : ZuulFilter() {

    override fun run(): Any? {
        val ctx = RequestContext.getCurrentContext()

        val context = SecurityContextHolder.getContext()
        if (context != null) {
            val authentication = context.authentication
            if (authentication != null && authentication is OAuth2Authentication) {
                ctx.addZuulRequestHeader("Authorization", "Bearer ${(authentication.details as OAuth2AuthenticationDetails).tokenValue}")
            }
        }

        return null
    }

    override fun shouldFilter() = true

    override fun filterType() = "pre"

    override fun filterOrder() = 1

}
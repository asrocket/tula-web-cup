package com.asrocket.tulawebcup.webapp

import org.springframework.cloud.stream.annotation.StreamListener
import org.springframework.cloud.stream.messaging.Sink
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Service

@Service
class UploadEventListener(val simpMessagingTemplate: SimpMessagingTemplate) {

    @StreamListener(Sink.INPUT)
    fun handleEvent(message: EventMessage) {
        simpMessagingTemplate.convertAndSendToUser(message.username, "/topic/uploadEvent", message)
    }
}
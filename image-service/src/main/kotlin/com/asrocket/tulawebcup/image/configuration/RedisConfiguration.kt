package com.asrocket.tulawebcup.image.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory
import org.springframework.data.redis.core.RedisTemplate

@Configuration
class RedisConfiguration {

    @Bean
    @ConfigurationProperties(prefix = "spring.redis")
    fun jedisConnectionFactory() = JedisConnectionFactory()

    @Bean
    fun redisTemplate(jedisConnectionFactory: JedisConnectionFactory): RedisTemplate<String, ByteArray> {
        val redisTemplate = RedisTemplate<String, ByteArray>()
        redisTemplate.setConnectionFactory(jedisConnectionFactory)
        return redisTemplate
    }
}
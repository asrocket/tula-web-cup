package com.asrocket.tulawebcup.image.configuration

import com.asrocket.tulawebcup.image.client.YandexUserInfoClient
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoRestTemplateFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter

@Configuration
@EnableResourceServer
class SecurityConfiguration(val sso: ResourceServerProperties,
                            val yandexUserInfoClient: YandexUserInfoClient) : ResourceServerConfigurerAdapter() {

    override fun configure(http: HttpSecurity) {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/images")
                    .permitAll()
                .anyRequest()
                    .authenticated()
    }

    @Bean
    @Primary
    fun yandexUserInfoTokenService(restTemplateFactory: UserInfoRestTemplateFactory) =
            YandexUserInfoTokenService(this.sso.clientId, yandexUserInfoClient)

}
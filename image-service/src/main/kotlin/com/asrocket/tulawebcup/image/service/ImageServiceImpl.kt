package com.asrocket.tulawebcup.image.service

import com.asrocket.tulawebcup.image.EventMessage
import com.asrocket.tulawebcup.image.ImageMessage
import com.asrocket.tulawebcup.image.client.YandexDiskClient
import com.asrocket.tulawebcup.image.db.mongodb.Image
import com.asrocket.tulawebcup.image.db.mongodb.ImageRepository
import org.slf4j.LoggerFactory
import org.springframework.cloud.stream.annotation.StreamListener
import org.springframework.cloud.stream.messaging.Sink
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.http.HttpEntity
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.support.MessageBuilder
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class ImageServiceImpl(val imageRepository: ImageRepository,
                       val redisTemplate: RedisTemplate<String, ByteArray>,
                       val yandexDiskClient: YandexDiskClient,
                       val output: MessageChannel) : ImageService {
    private val log = LoggerFactory.getLogger(this::class.java)
    private val imageNotFound = IllegalArgumentException("Image not found")
    private val restTemplate = RestTemplate()

    override fun geImagePage(tag: String?, offset: Int, limit: Int, order: String, orderType: String): Page<Image> {
        val pageRequest = PageRequest.of(offset, if (limit <= 0) 1000 else limit,
                Sort.by(Sort.Direction.valueOf(orderType.toUpperCase()), order))
        return if (tag == null) {
            imageRepository.getAllByAndShowIsTrue(pageRequest)
        } else {
            imageRepository.findByTagsInAndShowIsTrue(listOf(tag), pageRequest)
        }
    }

    override fun getImageBy(id: String): Image = imageRepository.findById(id)
            .orElseThrow { imageNotFound }


    override fun addOrRemoveLike(id: String, name: String): Image = imageRepository.findById(id)
            .map {
                if (it.like.contains(name)) {
                    it.like.remove(name)
                    it.likeCount--
                } else {
                    it.like.add(name)
                    it.likeCount++
                }
                return@map imageRepository.save(it)
            }
            .orElseThrow { imageNotFound }

    override fun addTag(id: String, tag: String, name: String): Image = imageRepository.findByIdAndAuthor(id, name)
            .map {
                if (tag.isBlank()) {
                    throw IllegalArgumentException("Tag blank value")
                }
                it.tags.add(tag.replace(" ", ""))
                return@map imageRepository.save(it)
            }
            .orElseThrow { imageNotFound }

    override fun getIdsBy(name: String, ids: List<String>) = imageRepository.findByLikeInAndIdIn(listOf(name), ids)
            .map { it.id!! }

    @StreamListener(Sink.INPUT)
    fun uploadProcess(message: ImageMessage) {
        val key = "images"
        val opsForHash = redisTemplate.opsForHash<String, ByteArray>()
        val imageData = opsForHash.get(key, message.id)
        if (imageData == null) {
            log.error("Image with id = ${message.id} not found in redis")

            return
        }

        val image = imageRepository.save(Image(message.name, message.author, message.mimeType))
        try {
            val response = yandexDiskClient.getUploadLink("app:/${image.id}", message.token)
            uploadFile(response.href, imageData)
            yandexDiskClient.publishResource("app:/${image.id}", message.token)
            val resource = yandexDiskClient.getResource("app:/${image.id}", message.token)
            val linkResponse = yandexDiskClient.downloadPublicResource(resource.publicUrl)
            image.link = linkResponse.href
            image.show = true
            imageRepository.save(image)
            opsForHash.delete(key, message.id)
            output.send(MessageBuilder.withPayload(EventMessage(message.author)).build())
        } catch (e: Exception) {
            log.error("Upload process error. {}", e.message)
            imageRepository.delete(image)
            opsForHash.delete(key, message.id)
            output.send(MessageBuilder.withPayload(EventMessage(message.author, false)).build())
        }
    }

    private fun uploadFile(link: String, data: ByteArray) {
        val requestEntity = HttpEntity(data)
        restTemplate.put(link, requestEntity)
    }
}
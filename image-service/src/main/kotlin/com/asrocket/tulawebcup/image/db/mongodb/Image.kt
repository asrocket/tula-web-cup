package com.asrocket.tulawebcup.image.db.mongodb

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document
class Image() {

    constructor(name: String, author: String, mimeType: String) : this() {
        this.name = name
        this.author = author
        this.mimeType = mimeType
    }

    @Id
    var id: String? = null
    var name: String = ""
    var author: String = ""
    var mimeType: String = ""
    var createdAt: LocalDateTime = LocalDateTime.now()
    var link: String = ""
    var show: Boolean = false

    var tags = mutableSetOf<String>()
    @JsonIgnore
    var like = mutableListOf<String>()

    var likeCount = 0
}
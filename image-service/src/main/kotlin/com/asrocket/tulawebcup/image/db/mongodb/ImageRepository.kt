package com.asrocket.tulawebcup.image.db.mongodb

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.*

interface ImageRepository : MongoRepository<Image, String> {

    fun findByTagsInAndShowIsTrue(tag: List<String>, pageable: Pageable): Page<Image>

    fun findByLikeInAndIdIn(users: List<String>, ids: List<String>): List<Image>

    fun getAllByAndShowIsTrue(pageable: Pageable): Page<Image>

    fun findByIdAndAuthor(id: String, author: String): Optional<Image>
}
package com.asrocket.tulawebcup.image.controller

import com.asrocket.tulawebcup.image.TagText
import com.asrocket.tulawebcup.image.service.ImageService
import org.springframework.web.bind.annotation.*
import java.security.Principal

@RestController
@RequestMapping("/images")
class ImageController(val imageService: ImageService) {

    @GetMapping
    fun getPage(
            @RequestParam(required = false) tag: String?,
            @RequestParam(defaultValue = "0", required = false) offset: Int,
            @RequestParam(defaultValue = "20", required = false) limit: Int,
            @RequestParam(defaultValue = "createdAt", required = false) order: String,
            @RequestParam(defaultValue = "desc", required = false) orderType: String
    ) = imageService.geImagePage(tag, offset, limit, order, orderType)

    @GetMapping("/{id}")
    fun getImage(@PathVariable id: String) = imageService.getImageBy(id)

    @PostMapping("/{id}/like")
    fun doLikeImage(@PathVariable id: String,
                    principal: Principal) = imageService.addOrRemoveLike(id, principal.name)

    @PostMapping("/{id}/tags")
    fun addTag(@PathVariable id: String,
               @RequestBody tag: TagText,
               principal: Principal) = imageService.addTag(id, tag.text, principal.name)
}
package com.asrocket.tulawebcup.image.service

import com.asrocket.tulawebcup.image.db.mongodb.Image
import org.springframework.data.domain.Page

interface ImageService {

    fun geImagePage(tag: String?, offset: Int, limit: Int, order: String, orderType: String): Page<Image>

    fun getImageBy(id: String): Image

    fun addOrRemoveLike(id: String, name: String): Image

    fun addTag(id: String, tag: String, name: String): Image

    fun getIdsBy(name: String, ids: List<String>): List<String>
}
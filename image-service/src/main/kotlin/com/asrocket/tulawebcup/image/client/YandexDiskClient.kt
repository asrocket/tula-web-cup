package com.asrocket.tulawebcup.image.client

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestParam

@FeignClient(name = "yandex-disk-client", url = "https://cloud-api.yandex.net/v1/disk")
interface YandexDiskClient {

    @GetMapping("/resources")
    fun getResources(@RequestParam limit: Int = 20,
                     @RequestParam path: String = "app:/",
                     @RequestHeader("Authorization") auth: String): ResourcesResponse

    @GetMapping("/resources")
    fun getResource(@RequestParam path: String = "app:/",
                    @RequestHeader("Authorization") auth: String): Image

    @GetMapping("/resources/upload")
    fun getUploadLink(@RequestParam path: String,
                      @RequestHeader("Authorization") auth: String): UploadResponse

    @PutMapping("/resources/publish")
    fun publishResource(@RequestParam path: String,
                        @RequestHeader("Authorization") auth: String): HrefResponse

    @GetMapping("/resources/download")
    fun downloadResource(@RequestParam path: String,
                         @RequestHeader("Authorization") auth: String): HrefResponse

    @GetMapping("/public/resources/download")
    fun downloadPublicResource(@RequestParam("public_key") publicKey: String): HrefResponse
}

data class ResourcesResponse(@JsonProperty("_embedded") val embedded: Items,
                             val name: String)

data class Items(val sort: String?, val items: List<Image>, val limit: Int, val offset: Int, val path: String, val total: Long)

data class Image(val name: String,
                 val file: String,
                 @JsonProperty("public_url") val publicUrl: String,
                 val preview: String)

data class UploadResponse(val href: String)

data class HrefResponse(val href: String, val method: String)
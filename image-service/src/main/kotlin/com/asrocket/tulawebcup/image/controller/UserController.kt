package com.asrocket.tulawebcup.image.controller

import com.asrocket.tulawebcup.image.service.ImageService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.security.Principal

@RestController
@RequestMapping("/users")
class UserController(val imageService: ImageService) {

    @GetMapping("/self/likes")
    fun getUserLikedImagesIds(@RequestParam("ids") ids: List<String>,
                              principal: Principal) = imageService.getIdsBy(principal.name, ids)

}